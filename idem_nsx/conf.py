CLI_CONFIG = {
    # pop-create options
    "services": {
        "subcommands": ["nsx"],
        "dyne": "pop_create",
    },
}
CONFIG = {
    # pop-create options
    "services": {
        "default": [],
        "nargs": "*",
        "help": "The cloud services to target, defaults to all",
        "dyne": "pop_create",
    },
}
SUBCOMMANDS = {
    "nsx": {
        "help": "Create idem_nsx state modules by parsing boto3",
        "dyne": "pop_create",
    },
}
DYNE = {
    "acct": ["acct"],
    "exec": ["exec"],
    "states": ["states"],
    "tool": ["tool"],
}
