def __init__(hub):
    # This enables acct profiles that begin with "nsx" for states
    hub.states.nsx.ACCT = ["nsx"]
